# -*- coding: utf-8 -*-
#
# docs documentation build configuration file, created by
# sphinx-quickstart on Tue Jun 25 07:37:37 2013.
#
# This file is execfile()d with the current directory set to its containing dir.
#
# Note that not all possible configuration values are present in this
# autogenerated file.
#
# All configuration values have a default; values that are commented out
# serve to show the default.

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import sys, os
#Pour le thème ReadTheDocs
#import sphinx_rtd_theme
#sys.path.insert(0, os.path.abspath('.'))

# -- General configuration -----------------------------------------------------

# If your documentation needs a minimal Sphinx version, state it here.
#needs_sphinx = '1.0'

# Add any Sphinx extension module names here, as strings. They can be extensions
# coming with Sphinx (named 'sphinx.ext.*') or your custom ones.

extensions = [
	'sphinx.ext.coverage',
	'sphinx.ext.mathjax',
	#'sphinxcontrib.bibtex',
	'sphinx.ext.intersphinx',
	'eqt_ext.eqt',
	'sphinx.ext.graphviz',
    "sphinx_design",
    "myst_parser"
    ]


#bibtex_bibfiles = ['']
#bibtex_bibliography_header = ""
#bibtex_cite_id = "cite-{bibliography_count}-{key}"
#bibtex_footcite_id = "footcite-{key}"
#bibtex_bibliography_id = "bibliography-{bibliography_count}"
#bibtex_footbibliography_id = "footbibliography-{footbibliography_count}"
#bibtex_default_style = 'apa'
#suppress_warnings = ["bibtex.duplicate_label", "bibtex.duplicate_citation"] # supprime les warnings des labels et des citations

# suppress_warnings = ["bibtex"]

myst_enable_extensions = [
    "amsmath",
    "colon_fence",
    "deflist",
    "dollarmath",
    "fieldlist",
    "html_admonition",
    "html_image",
    "linkify",
    "replacements",
    "smartquotes",
    "strikethrough",
    "substitution",
    "tasklist"
]

# Disable CSS loading when using sphinx tabs
#sphinx_tabs_disable_css_loading = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
# source_suffix = ['.rst', '.md'] ; Laissé à md pour forcer la compil des fichiers md
source_suffix = ['.md']

# The encoding of source files.
#
source_encoding = 'utf-8-sig'

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = u'Cours sur le numérique, Inspé, Univ. Grenoble Alpes'
#copyright = u'2022'
#author = u'test'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '1.0'
# The full version, including alpha/beta/rc tags.
release = '1.0'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = 'fr'

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#
#today = ''
#
# Else, today_fmt is used as the format for a strftime call.
#
today_fmt = '%d %B %Y'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# These patterns also affect html_static_path and html_extra_path
# exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
# ne pas lire le doc. qui permet de remplacer des termes.
exclude_patterns = ['inclure_infos.txt','src']


# Préférences MyST
# Donne le niveau de profondeur de création des ancres automatiques.
myst_heading_anchors = 2

# Substitutions ; texte à remplacer pour les admonitions.
myst_substitutions = {
  "phd": "[Philippe Dessus](http://pdessus.fr), Inspé & LaRAC, Univ. Grenoble Alpes",
  "citation": "**Citation** : Pour citer ce document : Auteur·s (Date_de_création_ou_de_révision). Titre_du_document. Grenoble : Univ. Grenoble Alpes, Inspé, base de cours en sciences de l'éducation, accédé le date_d_accès, URL_du_document.",
  "licence": "**Licence** : Document placé sous licence *Creative Commons* : [BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr).",
  "quizz": "Le quizz a été réalisé par Émilie Besse, projet [ReFlexPro](http://www.idefi-reflexpro.fr)."
}

# The reST default role (used for this markup: `text`) to use for all
# documents.
#
# default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#
# add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
#
# add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#
# show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
pygments_style = 'sphinx'

# A list of ignored prefixes for module index sorting.
# modindex_common_prefix = []

# If true, keep warnings as "system message" paragraphs in the built documents.
# keep_warnings = False

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = False

# Met en On la numérotation des Figures et spécifie leur format (voir https://github.com/sphinx-doc/sphinx/issues/1763), voir meilleur fonction dans myST

#numfig = True
#numfig_format = {'figure': 'Fig. %s', 'table': 'Tab. %s', 'code-block': 'Code %s'}

# Met la vérification de connexion sécurisée à false, pour permettre l'intersphinx

tls_verify = False

# Intersphinx mapping

intersphinx_mapping = {
    'reflexpro': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/reflexpro/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/reflexpro/objects.inv'),
    'num': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/num/objects.inv'),
    'sciedu-ateliers': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/ateliers/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/ateliers/objects.inv'),
    'banque': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/banque/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/banque/objects.inv'),
    'gene': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/gene/objects.inv'),
    'qcm': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/qcm/objects.inv'),
    'rech-educ': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/rech-educ/objects.inv'),
    'peda-univ': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/peda-univ/objects.inv'),
    'inspe-test': ('https://pdessus.gricad-pages.univ-grenoble-alpes.fr/doc-inspe-test/','https://pdessus.gricad-pages.univ-grenoble-alpes.fr/doc-inspe-test/objects.inv'),
    'cours-class': ('https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/','https://inspe-sciedu.gricad-pages.univ-grenoble-alpes.fr/class/objects.inv')
}








# -- Options for HTML output ---------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
#html_theme = 'sphinx_rtd_theme'
html_theme = "sphinx_book_theme"

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation. # Spécifiques à Sphinx-book
html_theme_options = {
    "home_page_in_toc": True,
    "repository_url": "https://gricad-gitlab.univ-grenoble-alpes.fr/inspe-sciedu/ateliers",
    "use_repository_button": True,
    "use_download_button": True
}



# Add any paths that contain custom themes here, relative to this directory.
# pour le Read_The_docs
#html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
#
html_title = "Ateliers, Inspé, Univ. Grenoble Alpes"

# A shorter title for the navigation bar.  Default is the same as html_title.
#
# html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
#
#html_logo = 'images/logo-espe-uga.png'

# The name of an image file (relative to this directory) to use as a favicon of
# the docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#
# html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# ajout pour que qcm-ext fonctionne
from eqt_ext import get_eqt_ext_static_dir
html_static_path = ['public/_static', get_eqt_ext_static_dir()]


# Add any extra paths that contain custom files (such as robots.txt or
# .htaccess) here, relative to this directory. These files are copied
# directly to the root of the documentation.
#
# html_extra_path = []

# If not None, a 'Last updated on:' timestamp is inserted at every page
# bottom, using the given strftime format.
# The empty string is equivalent to '%b %d, %Y'.
#
# html_last_updated_fmt = None

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
# html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
#
html_sidebars = {
'**': ['sidebar-logo.html', 'globaltoc.html', 'searchbox.html', 'sourcelink'],
}

# Additional templates that should be rendered to pages, maps page names to
# template names.
#
# html_additional_pages = {}

# If false, no module index is generated.
#
# html_domain_indices = True

# If false, no index is generated.
#
html_use_index = True

# If true, the index is split into individual pages for each letter.
#
# html_split_index = False

# If true, links to the reST sources are added to the pages.
#
# html_show_sourcelink = True

# If true, "Created using Sphinx" is shown in the HTML footer. Default is True.
#
# html_show_sphinx = True

# If true, "(C) Copyright ..." is shown in the HTML footer. Default is True.
#
# html_show_copyright = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#
# html_use_opensearch = ''

# This is the file name suffix for HTML files (e.g. ".xhtml").
# html_file_suffix = None

# Language to be used for generating the HTML full-text search index.
# Sphinx supports the following languages:
#   'da', 'de', 'en', 'es', 'fi', 'fr', 'hu', 'it', 'ja'
#   'nl', 'no', 'pt', 'ro', 'ru', 'sv', 'tr', 'zh'
#
html_search_language = 'fr'

# A dictionary with options for the search language support, empty by default.
# 'ja' uses this config value.
# 'zh' user can custom change `jieba` dictionary path.
#
# html_search_options = {'type': 'default'}

# The name of a javascript file (relative to the configuration directory) that
# implements a search results scorer. If empty, the default will be used.
#
# html_search_scorer = 'scorer.js'

# Output file base name for HTML help builder.
htmlhelp_basename = 'testsdoc'

# -- Options for LaTeX output --------------------------------------------------

latex_engine = 'xelatex'

sd_fontawesome_latex = True # Permet l'utilisation des icones sphinx-design dans latex

latex_elements = {
# The paper size ('letterpaper' or 'a4paper').
'papersize': 'a4paper',
'preamble': r'''
\makeatletter
\renewenvironment{thebibliography}[1]
    {\list{\@biblabel{\@arabic\c@enumiv}}%
        {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
    \sloppy
    \clubpenalty4000
    \@clubpenalty \clubpenalty
    \widowpenalty4000%
    \sfcode`\.\@m}
    {\def\@noitemerr
    {\@latex@warning{Empty `thebibliography' environment}}%
    \endlist}
\makeatother
\renewenvironment{sphinxthebibliography}[1]
    {\begin{thebibliography}{#1}}
    {\end{thebibliography}}

 '''

    # The font size ('10pt', '11pt' or '12pt').
    #
    # 'pointsize': '10pt',

    # Additional stuff for the LaTeX preamble.
    #
    # 'preamble': '',

    # Latex figure (float) alignment
    #
    # 'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
  ('index', 'docs.tex', u'Cours en sciences de l\'éducation',
   u'Inspé, Univ. Grenoble Alpes', 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
#
# latex_logo = None

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
#latex_use_parts = 'False'

# If true, show page references after internal links.
#
latex_show_pagerefs = True

# If true, show URL addresses after external links.
#
# latex_show_urls = False

# Documents to append as an appendix to all manuals.
#
# latex_appendices = []

# If false, no module index is generated.
#
# latex_domain_indices = True


# -- Options for manual page output ---------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'docs', u'docs',
     [u'Philippe Dessus'], 1)
]

# If true, show URL addresses after external links.
#
# man_show_urls = False


# -- Options for Texinfo output -------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
  ('index', 'docs', u'docs',
   u'Philippe Dessus', 'docs', 'One line description of project.',
   'Miscellaneous'),
]

# Documents to append as an appendix to all manuals.
#
# texinfo_appendices = []

# If false, no module index is generated.
#
# texinfo_domain_indices = True

# How to display URL addresses: 'footnote', 'no', or 'inline'.
#
# texinfo_show_urls = 'footnote'

# If true, do not generate a @detailmenu in the "Top" node's menu.
#
# texinfo_no_detailmenu = False

# If false, do not generate in manual @ref nodes.
#
# texinfo_cross_references = False


# -- Options for Epub output ---------------------------------------------------

# Bibliographic Dublin Core info.
epub_basename = 'Ateliers'
epub_title = project
epub_author = u'Inspé, Univ. Grenoble Alpes'
epub_publisher = u'Inspé, Univ. Grenoble ALpes'
epub_copyright = u'BY-NC-SA, 2022, Univ. Grenoble Alpes'
epub_theme = u'epub'

# The language of the text. It defaults to the language option
# or en if the language is not set.
#epub_language = ''

# The scheme of the identifier. Typical schemes are ISBN or URL.
#epub_scheme = ''

# The unique identifier of the text. This can be a ISBN number
# or the project homepage.
#epub_identifier = ''

# A unique identification for the text.
#epub_uid = ''

# A tuple containing the cover image and cover page html template filenames.
#epub_cover = ()

# HTML files that should be inserted before the pages created by sphinx.
# The format is a list of tuples containing the path and title.
#epub_pre_files = []

# HTML files shat should be inserted after the pages created by sphinx.
# The format is a list of tuples containing the path and title.
#epub_post_files = []

# A list of files that should not be packed into the epub file.
#epub_exclude_files = ['searchbox.html']

# The depth of the table of contents in toc.ncx.
#epub_tocdepth = 3

# Allow duplicate toc entries.
#epub_tocdup = True


numfig = True



from docutils import nodes
#from docutils.nodes import Element, Node
from sphinx.transforms.post_transforms import SphinxPostTransform
from sphinx.builders.latex.nodes import thebibliography
from typing import Any
# from sphinx.util.logging import getLogger
# logger = getLogger(__name__)

#class CustomBibliographyTransform(SphinxPostTransform):
#    default_priority = 750
#    formats = ('latex',)

#    def run(self, **kwargs: Any) -> None:
#        citations = thebibliography()
#        prev_parent = None
#        #logger.info(self.document)
#        for node in self.document.traverse(nodes.citation):
#            old_parent = node.parent
#            #logger.info(f"old_parent {old_parent}")
#            if node.parent == prev_parent or prev_parent == None:
#                citations += node
#                #node.parent.remove(node)
#                #logger.info(f"add citation {node}")
#            else:
#                #self.document.replace(prev_parent,citations)
#                #logger.info(f"new citation {node}")
#                prev_parent.replace_self(citations)
#                citations = thebibliography()
#                citations += node
#                #logger.info("reinit")
#            prev_parent = old_parent
#        prev_parent.replace_self(citations)
#        #logger.info(f"newdoc {self.document}")

#def setup(app):
#    app.add_js_file('custom.js')
#    for trans in app.registry.get_post_transforms():
#        if str(trans) == "<class 'sphinx.builders.latex.transforms.BibliographyTransform'>":
#            app.registry.post_transforms.remove(trans)
#    app.add_post_transform(CustomBibliographyTransform)
  

